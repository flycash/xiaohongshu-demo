package main

import "github.com/gin-gonic/gin"

func main() {
	server := gin.Default()
	server.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	server.POST("/post", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "这是一个 POST 方法",
		})
	})
	// ip:port
	// 监听并在 0.0.0.0:8080 上启动服务
	if err := server.Run(); err != nil {
		panic(err)
	}
}
