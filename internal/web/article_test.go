package web

import (
	"bytes"
	"gitee.com/flycash/xiaohongshu-demo/internal/service"
	"gitee.com/flycash/xiaohongshu-demo/internal/service/mocks"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestArticleController_Save(t *testing.T) {
	testCases := []struct {
		name string

		mock func(ctrl *gomock.Controller) service.ArticleService

		req *http.Request

		wantCode int
		wantBody string
	}{
		{
			name: "保存成功",
			mock: func(ctrl *gomock.Controller) service.ArticleService {
				svc := mocks.NewMockArticleService(ctrl)
				svc.EXPECT().Save(gomock.Any(), gomock.Any()).
					Return(int64(1), nil)
				return svc
			},
			req: func() *http.Request {
				req, _ := http.NewRequest(http.MethodPost,
					"/article/new", bytes.NewBuffer([]byte(`{
"title":"这是标题",
"content": "这是内容"
}`)))
				req.Header.Set("Content-Type", "application/json")
				return req
			}(),
			wantCode: http.StatusOK,
			wantBody: "1",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			svc := tc.mock(ctrl)

			artController := NewArticleController(svc)
			server := gin.Default()
			artController.RegisterRoutes(server)
			// 收集你准备返回给前端的响应
			w := httptest.NewRecorder()
			server.ServeHTTP(w, tc.req)

			//artController.Save()
			// 断言
			// 断言 http 状态码
			assert.Equal(t, tc.wantCode, w.Code)
			assert.Equal(t, tc.wantBody, w.Body.String())
		})
	}
}
