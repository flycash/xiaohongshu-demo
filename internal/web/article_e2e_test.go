package web

import (
	"bytes"
	"gitee.com/flycash/xiaohongshu-demo/internal/repository"
	"gitee.com/flycash/xiaohongshu-demo/internal/repository/cache"
	"gitee.com/flycash/xiaohongshu-demo/internal/repository/dao"
	"gitee.com/flycash/xiaohongshu-demo/internal/service"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestArticleController_Save_e2e(t *testing.T) {
	bDB, err := gorm.Open(mysql.Open("root:root@tcp(localhost:13316)/xiaohongshu_b"))
	if err != nil {
		panic(err)
	}
	dao.Init(bDB)

	server := gin.Default()
	bArtDAO := dao.NewArticleDAO(bDB)
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       1,
	})
	artCache := cache.NewRedisArticleCache(redisClient)
	bArtRepo := repository.NewArticleRepository(bArtDAO, artCache)
	artSvc := service.NewArticleService(bArtRepo, nil)
	artCtrl := NewArticleController(artSvc)
	artCtrl.RegisterRoutes(server)

	testCases := []struct {
		name string

		// 准备数据
		before func()
		// 清除数据
		after func()

		req *http.Request

		wantCode int
		wantBody string
	}{
		{
			name: "保存成功",
			before: func() {
				bDB.Exec("TRUNCATE TABLE articles;")
			},
			after: func() {
				bDB.Exec("TRUNCATE TABLE articles;")
			},
			req: func() *http.Request {
				req, _ := http.NewRequest(http.MethodPost,
					"/article/new", bytes.NewBuffer([]byte(`{
"title":"这是标题",
"content": "这是内容"
}`)))
				req.Header.Set("Content-Type", "application/json")
				return req
			}(),
			wantCode: http.StatusOK,
			wantBody: "1",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.before()
			defer tc.after()
			// 收集你准备返回给前端的响应
			w := httptest.NewRecorder()
			server.ServeHTTP(w, tc.req)

			//artController.Save()
			// 断言
			// 断言 http 状态码
			assert.Equal(t, tc.wantCode, w.Code)
			assert.Equal(t, tc.wantBody, w.Body.String())
		})
	}
}
