package service

import (
	"context"
	"errors"
	"gitee.com/flycash/xiaohongshu-demo/internal/domain"
	"gitee.com/flycash/xiaohongshu-demo/internal/repository"
	"gitee.com/flycash/xiaohongshu-demo/internal/repository/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestArticleService_Save(t *testing.T) {
	testCases := []struct {
		// 名字
		name string

		// 模拟数据
		mock func(ctrl *gomock.Controller) repository.ArticleRepository

		// 输入
		ctx     context.Context
		article domain.Article

		// 预期数据
		wantId  int64
		wantErr error
	}{
		{
			name: "创建",
			ctx:  context.Background(),
			mock: func(ctrl *gomock.Controller) repository.ArticleRepository {
				bRepo := mocks.NewMockArticleRepository(ctrl)
				bRepo.EXPECT().Create(context.Background(), domain.Article{
					Id:    0,
					Title: "这是标题",
				}).Return(int64(1), nil)
				return bRepo
			},
			article: domain.Article{
				Id:    0,
				Title: "这是标题",
			},
			wantId:  1,
			wantErr: nil,
		},
		{
			name: "更新",
			ctx:  context.Background(),
			mock: func(ctrl *gomock.Controller) repository.ArticleRepository {
				bRepo := mocks.NewMockArticleRepository(ctrl)
				bRepo.EXPECT().Update(context.Background(), domain.Article{
					Id:    3,
					Title: "这是标题",
				}).Return(nil)
				return bRepo
			},
			article: domain.Article{
				Id:    3,
				Title: "这是标题",
			},
			wantId:  3,
			wantErr: nil,
		},

		{
			name: "更新失败",
			ctx:  context.Background(),
			mock: func(ctrl *gomock.Controller) repository.ArticleRepository {
				bRepo := mocks.NewMockArticleRepository(ctrl)
				bRepo.EXPECT().Update(context.Background(), domain.Article{
					Id:    3,
					Title: "这是标题",
				}).Return(errors.New("更新失败"))
				return bRepo
			},
			article: domain.Article{
				Id:    3,
				Title: "这是标题",
			},
			wantId:  3,
			wantErr: errors.New("更新失败"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// 准备模拟数据
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			bRepo := tc.mock(ctrl)

			// 构造实例
			service := NewArticleService(bRepo, nil)

			// 发起目标调用
			id, err := service.Save(tc.ctx, tc.article)

			// 断言结果符合你的预期
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantId, id)
		})
	}
}
