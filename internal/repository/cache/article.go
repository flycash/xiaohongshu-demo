package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/flycash/xiaohongshu-demo/internal/domain"
	"github.com/go-redis/redis"
	"time"
)

type ArticleCache interface {
	Set(ctx context.Context, article domain.Article) error
}

type redisArticleCache struct {
	client     redis.Cmdable
	pattern    string
	expiration time.Duration
}

func NewRedisArticleCache(client redis.Cmdable) ArticleCache {
	return &redisArticleCache{
		client:     client,
		pattern:    "article_%d",
		expiration: time.Minute * 15,
	}
}

func (r *redisArticleCache) Set(ctx context.Context, article domain.Article) error {
	vals, err := json.Marshal(article)
	if err != nil {
		return err
	}
	_, err = r.client.Set(fmt.Sprintf(r.pattern, article.Id), vals, r.expiration).Result()
	return err
}
