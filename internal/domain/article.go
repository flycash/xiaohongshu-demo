package domain

type Article struct {
	Id      int64
	Author  int64
	Title   string
	Content string
}
