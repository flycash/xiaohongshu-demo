package main

import "fmt"

func main() {
	a1 := [3]int{11, 12, 13}
	fmt.Printf("a1: %v len: %d, cap: %d \n", a1, len(a1), cap(a1))
	a2 := [3]int{11, 12}
	fmt.Printf("a2: %v len: %d, cap: %d \n", a2, len(a2), cap(a2))

	var a3 [3]int
	fmt.Printf("a3: %v len: %d, cap: %d \n", a3, len(a3), cap(a3))

	a1[1] = 25
	println(a1[1])
	//a1 = append(a1, 4)
}

//func CreateArray(cap int) {
//	var arr [cap]int
//	println(arr)
//}
