package main

import "fmt"

type Fish struct {
	name string
}

func (f Fish) Swim() {
	fmt.Printf("鱼儿游呀游")
}

// 衍生类型
type FakeFish Fish

func (f FakeFish) Swim() {
	fmt.Printf("华强北山寨鱼游呀游" + f.name)
}

func (f FakeFish) Swim1() {
	fmt.Printf("华强北山寨鱼游呀游")
}

type StrongFish Fish
