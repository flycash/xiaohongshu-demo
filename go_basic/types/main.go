package main

import (
	"fmt"
	"gitee.com/flycash/xiaohongshu-demo/go_basic/types/list"
	"gitee.com/flycash/xiaohongshu-demo/go_basic/types/news"
)

func main() {
	fish := Fish{}
	fish.Swim()
	fakeFish := FakeFish{}
	fakeFish.Swim()
	//fakeFish = fish
	fakeFish = FakeFish(fish)
	fakeFish.Swim()

	println()

	ns := news.News{}
	var fakeNews FakeNews = ns
	fmt.Printf("%+v \n", fakeNews)

	var fish2 Fish
	fmt.Printf("%+v \n", fish2)
	fish2.Swim()
	// 这个是 nil
	var fish3 *Fish
	// 这个会 panic
	// fish3.Swim()
	fmt.Printf("%+v \n", fish3)

	duck1 := ToyDuck{
		Color: "黄色",
		Price: 100,
	}
	fmt.Printf("%+v \n", duck1)

	duck2 := ToyDuck{}
	duck2.Color = "黄色"
	fmt.Printf("%+v \n", duck2)

	var duck3 *ToyDuck
	// 这边会panic
	// duck3.Color = "黑色"
	fmt.Printf("%+v \n", duck3)

	duck4 := &ToyDuck{}
	// 解引用
	duck2 = *duck4

	// user

	u1 := User{
		Name: "Tom",
		Age:  18,
	}
	fmt.Printf("%+v \n", u1)
	fmt.Printf("u1 address %p \n", &u1)

	u1.ChangeName("Jerry")
	u1.ChangeAge(35)
	fmt.Printf("%+v \n", u1)

	u2 := &User{
		Name: "小明",
		Age:  18,
	}

	fmt.Printf("%+v \n", u2)
	fmt.Printf("u2 address %p \n", u2)

	u2.ChangeName("Jerry")
	u2.ChangeAge(35)
	fmt.Printf("%+v \n", u2)

	list.UseTypeP()
}

type FakeNews = news.News
