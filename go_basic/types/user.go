package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func (u User) ChangeName(name string) {
	fmt.Printf("u address %p \n", &u)
	u.Name = name
}

func (u *User) ChangeAge(age int) {
	u.Age = age
}
