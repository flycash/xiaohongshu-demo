package main

import "gitee.com/flycash/xiaohongshu-demo/go_basic/variables/external"

var a int

var (
	b int32 = 123
	c       = 456
	d       = "hello"
	e       = 12.3
)

func main() {
	println(a)

	var a string = "hello"
	println(a)

	var f = 12
	println(f)

	println(external.B)

	g := 123
	var h int64 = 123
	println(g)
	println(h)

	//var external = "hllo"
	//external.
}

const a1 = 123

const (
	b1 int32 = 123
	c1       = 123
)

const (
	d1 = iota
	d2
	d3
)

const (
	d4 = iota
	//MY_NAME = 345
	//myName = 345
)
