package main

import (
	"unicode/utf8"
)

func main() {
	str1 := "He said: \"hello, world\""
	str2 := `
{
	"key1": "vaule1"
}
`
	println(str1)
	println(str2)

	str3 := "abc"
	// 输出 3
	println(len(str3))
	str4 := "你好"
	// 输出了 6
	println(len(str4))
	// len 输出的是字节长度，不是字符个数
	// 下面输出 2
	println(utf8.RuneCountInString(str4))

	str5 := "你好abc"
	println(utf8.RuneCountInString(str5))

	str6 := str4 + str5
	println(str6)
	//str7 := "hello, " + 123

	var a1 int32 = 12
	var a2 int64 = int64(a1)
	println(a2)
}

// He said: "hello, world"
