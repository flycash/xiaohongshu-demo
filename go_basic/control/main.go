package main

import (
	"fmt"
)

func main() {
	//ForLoop()
	ForLoop3()
}

func ForLoop() {
	i := 0
	for {
		if i > 3 {
			break
		}
		println("hello, go")
		// 自增 1
		i++
	}
	println("end...")
}

func ForLoop1() {
	for i := 0; i < 10; i++ {
		println("hello, go")
	}
}

func ForLoop2() {
	arr := []int{10, 11, 12, 13}
	for idx, ele := range arr {
		if idx%2 == 1 {
			// 奇数下标就跳过
			continue
		}
		fmt.Printf("idx: %d, ele: %d \n", idx, ele)
	}

	for idx := range arr {
		fmt.Printf("idx: %d, ele: %d \n", idx, arr[idx])
	}

	for _, ele := range arr {
		fmt.Printf("ele: %d \n", ele)
	}
}

func ForLoop3() {
	i := 0
	for i <= 3 {
		println("hello, go")
		// 自增 1
		i++
	}
}

func IfElse(age int) string {
	if age < 18 {
		return "年轻人"
	} else if age < 35 {
		return "还能找到工作"
	} else {
		return "要被裁员啦"
	}
}

func IfUsingNewVariable(start int, end int) {
	if distance := end - start; distance > 100 {
		fmt.Printf("距离太远，不来了 %d", distance)
	} else {
		fmt.Printf("距离很近，不来了 %d", distance)
	}
}

func SwitchCase(fruit string) {
	switch fruit {
	case "苹果":
		println("这是牛顿")
	case "橘子":
		println("太酸了")
	default:
		println("这是默认分支")
	}
}
