package main

func main() {
	sum, _ := Sum(1, 2)
	println(sum)
	_, _ = Sum1(1, 2)

	sum, err1 := Sum1(1, 2)
	println(err1)
}

func Sum(a, b int) (sum int, err error) {
	return a + b, nil
}

func Sum1(a, b int) (sum int, err error) {
	sum = a + b
	return
}

func Sum2(a, b int) (int, error) {
	return a + b, nil
}

func Sum3(int, int) (int, error) {
	return 0, nil
}
