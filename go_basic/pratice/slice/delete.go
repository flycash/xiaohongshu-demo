// Copyright 2021 ecodeclub
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package slice

// Delete 删除 index 处的元素
func Delete[Src any](src []Src, index int) ([]Src, error) {
	panic("implement me")
}

// FilterDelete 删除符合条件的元素
// 以下不做要求，但是你可以尝试一下
// 考虑到性能问题，所有操作都会在原切片上进行
// 被删除元素之后的元素会往前移动，有且只会移动一次
func FilterDelete[Src any](src []Src, m func(idx int, src Src) bool) []Src {
	panic("implement me")
}
