// Copyright 2021 ecodeclub
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package list

var (
	_ List[any] = &ArrayList[any]{}
)

// ArrayList 基于切片的简单封装
type ArrayList[T any] struct {
	vals []T
}

// NewArrayList 初始化一个len为0，cap为cap的ArrayList
func NewArrayList[T any](cap int) *ArrayList[T] {
	panic("implement me")
}

// NewArrayListOf 直接使用 ts，而不会执行复制
func NewArrayListOf[T any](ts []T) *ArrayList[T] {
	panic("implement me")
}

func (a *ArrayList[T]) Get(index int) (t T, e error) {
	panic("implement me")
}

// Append 往ArrayList里追加数据
func (a *ArrayList[T]) Append(ts ...T) error {
	panic("implement me")
}

// Add 在ArrayList下标为index的位置插入一个元素
// 当index等于ArrayList长度等同于append
func (a *ArrayList[T]) Add(index int, t T) error {
	panic("implement me")
}

// Set 设置ArrayList里index位置的值为t
func (a *ArrayList[T]) Set(index int, t T) error {
	panic("implement me")
}

// Delete 删除所在位置的元素
func (a *ArrayList[T]) Delete(index int) (T, error) {
	panic("implement me")
}

func (a *ArrayList[T]) Len() int {
	panic("implement me")
}

func (a *ArrayList[T]) Cap() int {
	panic("implement me")
}

func (a *ArrayList[T]) Range(fn func(index int, t T) error) error {
	panic("implement me")
}

// AsSlice 每次调用都必须返回一个全新的切片
func (a *ArrayList[T]) AsSlice() []T {
	panic("implement me")
}
