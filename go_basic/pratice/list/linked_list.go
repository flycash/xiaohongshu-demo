// Copyright 2021 ecodeclub
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package list

var (
	_ List[any] = &LinkedList[any]{}
)

// node 双向循环链表结点
type node[T any] struct {
	prev *node[T]
	next *node[T]
	val  T
}

// LinkedList 双向循环链表
type LinkedList[T any] struct {
	head   *node[T]
	tail   *node[T]
	length int
}

// NewLinkedList 创建一个双向循环链表
func NewLinkedList[T any]() *LinkedList[T] {
	panic("implement me")
}

// NewLinkedListOf 将切片转换为双向循环链表, 直接使用了切片元素的值，而没有进行复制
func NewLinkedListOf[T any](ts []T) *LinkedList[T] {
	panic("implement me")
}

func (l *LinkedList[T]) Get(index int) (T, error) {
	panic("implement me")
}

func (l *LinkedList[T]) checkIndex(index int) bool {
	panic("implement me")
}

// Append 往链表最后添加元素
func (l *LinkedList[T]) Append(ts ...T) error {
	panic("implement me")
}

// Add 在 LinkedList 下标为 index 的位置插入一个元素
// 当 index 等于 LinkedList 长度等同于 Append
func (l *LinkedList[T]) Add(index int, t T) error {
	panic("implement me")
}

// Set 设置链表中index索引处的值为t
func (l *LinkedList[T]) Set(index int, t T) error {
	panic("implement me")
}

// Delete 删除指定位置的元素
func (l *LinkedList[T]) Delete(index int) (T, error) {
	panic("implement me")
}

func (l *LinkedList[T]) Len() int {
	panic("implement me")
}

func (l *LinkedList[T]) Cap() int {
	panic("implement me")
}

func (l *LinkedList[T]) Range(fn func(index int, t T) error) error {
	panic("implement me")
}

func (l *LinkedList[T]) AsSlice() []T {
	panic("implement me")
}
